#------------------------------------------------------------------------------------------
# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force;
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

if ($LASTEXITCODE -ne 0) {
    Write-Host "ERROR: Error installing chocolatey"
    exit 1;
}

Write-Host "STATUS: Installed chocolatey"

#------------------------------------------------------------------------------------------
# Install Git

choco install git

if ($LASTEXITCODE -ne 0) {
    Write-Host "ERROR: Error installing Git"
    exit 1;
}

Write-Host "STATUS: Installed Git"

#------------------------------------------------------------------------------------------
# Git Config

git config --global core.preloadindex true
git config --global core.fscache true
git config --global gc.auto 256
git config --global core.autocrlf false

#------------------------------------------------------------------------------------------
# SSH

Set-Service -Name ssh-agent -StartupType Automatic
Start-Service -Name ssh-agent
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
Set-Service -Name sshd -StartupType Automatic
Start-Service -Name sshd
ssh-keygen -C qemu.win

#------------------------------------------------------------------------------------------
# Misc

New-Item -ItemType Directory -Path "C:\data\"
New-Item -ItemType Directory -Path "C:\data\sources"
Add-Content -Path "C:\Windows\System32\drivers\etc\hosts" -Value "192.168.1.145 nas"
#------------------------------------------------------------------------------------------
